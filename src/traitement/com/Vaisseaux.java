package traitement.com;

public class Vaisseaux {
	
	private String name;

	private int totalDegat;
	
	private int degatSousBouclier;
	
	private int degatSurCoque;
	
	private int degatSurBouclier;
	
	private int compteurShootCoque;
	
	private int compteurShootBouclier;
	
	private int compteurShootCoqueSousBouclier;
	
	private int moyenne;
	
	private int moyenneTotal;
	
	private int moyenneSurCoque;
	
	private int moyenneBouclier;
	
	private int maxShootCoque;
	
	private int maxShootBouclier;

	
	/**
	 * 
	 * @param name
	 * @param coque
	 */
	public Vaisseaux(String name, int coque) {
		
		setCompteurShootCoque();
		this.name = name;
		this.totalDegat = coque;
		maxShootCoque= coque;
		moyenneSurCoque = coque;
		
	}
	/**
	 * 
	 * @param name
	 * @param moyenne
	 * @param bouclier
	 */
	public Vaisseaux(String name, int moyenne,int bouclier) {
		setCompteurShootCoque();
		setCompteurShootBouclier();
		this.name = name;
		maxShootBouclier = bouclier;
		maxShootCoque= moyenne;

		setDegatSousBouclier(moyenne);
		setDegatSurBouclier(bouclier);
		
	}
	

	

	public void upgradeDegatByRefresh(int degatCoque) {
		
		
		if(degatCoque > maxShootCoque)
			maxShootCoque = degatCoque;
		
		setDegatSurCoque(degatCoque);
	}
	
	public void upgradeDegatByRefresh(int degatCoqueBouclier, int bouclier) {
		
		
		if(degatCoqueBouclier > maxShootCoque) {	
			maxShootCoque = degatCoqueBouclier;
		}
		
		
		if(bouclier > maxShootBouclier) {
		
			maxShootBouclier = bouclier;
		}
		
		
	//	setCompteurShootCoqueSousBouclier();
		
		setDegatSousBouclier(degatCoqueBouclier);
		setDegatSurBouclier(bouclier);
	}
	
	@Override
	public String toString() {
		return "Vaisseaux [name=" + name + ", moyenne=" + moyenne + ", totalDegat=" + totalDegat
				+ ", degatSousBouclier=" + degatSousBouclier + ", degatSurCoque=" + getDegatSurCoque()
				+ ", degatSurBouclier=" + getDegatSurBouclier() + ", compteurShoot=" ;
			
	}

	public int getDegatSousBouclier() {
		System.out.println(getCompteurShootCoqueSousBouclier() + " " + degatSousBouclier);
		int calc;
		try {
			calc = degatSousBouclier / getCompteurShootCoqueSousBouclier();
		} catch (ArithmeticException e) {
			calc = 0;
		}
		return calc;
	}

	public void setDegatSousBouclier(int degatSousBouclier) {
		setCompteurShootCoqueSousBouclier();
		totalDegat+=degatSousBouclier;
		this.degatSousBouclier += degatSousBouclier;
	}

	public int getDegatSurCoque() {
		return degatSurCoque;
	}

	public void setDegatSurCoque(int degatSurCoque) {
		setCompteurShootCoque();
		setMoyenneSurCoque(degatSurCoque);
		totalDegat+=degatSurCoque;
		this.degatSurCoque += degatSurCoque;
	}

	public int getDegatSurBouclier() {
		return degatSurBouclier;
	}

	public void setDegatSurBouclier(int degatSurBouclier) {
		setCompteurShootBouclier();
		setMoyenneBouclier(degatSurBouclier);
		totalDegat+= degatSurBouclier;
		this.degatSurBouclier += degatSurBouclier;
	}

	public String getName() {
		
		return name;
	}
	
	public void setName(String nom) {
		
		name = nom;
	}

	public int getCompteurShootCoque() {
		return compteurShootCoque;
	}

	public void setCompteurShootCoque() {
		this.compteurShootCoque+=1;
	}

	public int getMoyenneTotal() {
		return moyenneTotal;
	}

	public void setMoyenneTotal() {
		this.moyenneTotal +=1;
	}

	public int getCompteurShootBouclier() {
		return compteurShootBouclier;
	}

	public void setCompteurShootBouclier() {
		this.compteurShootBouclier+=1;
	}

	public int getMoyenneBouclier() {
		int calc;
		try {
			calc = moyenneBouclier / getCompteurShootBouclier();
		} catch (Exception e) {
			calc = 0;
		}
		return calc;
	}

	public void setMoyenneBouclier(int moyenneBouclier) {
		this.moyenneBouclier += moyenneBouclier;
	}

	public int getMoyenneSurCoque() {
		return moyenneSurCoque / compteurShootCoque;
	}

	public int getTotalDegat() {
		return totalDegat;
	}

	public int getMaxShootCoque() {
		return maxShootCoque;
	}

	public int getMaxShootBouclier() {
		return maxShootBouclier;
	}

	public void setCompteurShootCoque(int compteurShootCoque) {
		this.compteurShootCoque = compteurShootCoque;
	}

	public void setMoyenneSurCoque(int moyen) {
		this.moyenneSurCoque+=moyen;
	}
	
	public int getCompteurShootCoqueSousBouclier() {
		return compteurShootCoqueSousBouclier;
	}

	public void setCompteurShootCoqueSousBouclier() {
		this.compteurShootCoqueSousBouclier += 1;
	}

	
}
