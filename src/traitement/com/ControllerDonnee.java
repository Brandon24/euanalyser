package traitement.com;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;

public class ControllerDonnee implements Initializable {
	
	@FXML
	private TextArea Log;
	@FXML
	private Button Validation;
	@FXML
	private TableView<Vaisseaux> tableauInformation;
	@FXML
	private TableColumn<Vaisseaux, String> columNom;
	@FXML
	private TableColumn<Vaisseaux, Integer> columnBouclierMoyenne;
	@FXML
	private TableColumn<Vaisseaux, Integer> columnBouclierShootMax;
	@FXML
	private TableColumn<Vaisseaux, Integer> columnBouclierTotal;
	@FXML
	private TableColumn<Vaisseaux, Integer> columnCoqueSousBouclier;
	@FXML
	private TableColumn<Vaisseaux, Integer> columnCoqueMoyenne;
	@FXML
	private TableColumn<Vaisseaux, Integer> columnCoqueShootMax;
	@FXML
	private TableColumn<Vaisseaux, Integer> columnCoqueTotal;
	@FXML
	private TableColumn<Vaisseaux, Integer> columnTotal;
	
	private ObservableList<Vaisseaux> listeVaisseaux = FXCollections.observableArrayList();
	
	private static Tri tri = new Tri();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
		columNom.setCellValueFactory(new PropertyValueFactory<Vaisseaux,String>("Name"));
		columnBouclierMoyenne.setCellValueFactory(new PropertyValueFactory<Vaisseaux,Integer>("MoyenneBouclier"));
		columnBouclierShootMax.setCellValueFactory(new PropertyValueFactory<Vaisseaux,Integer>("MaxShootBouclier"));
		columnBouclierTotal.setCellValueFactory(new PropertyValueFactory<Vaisseaux,Integer>("DegatSurBouclier"));
		columnCoqueSousBouclier.setCellValueFactory(new PropertyValueFactory<Vaisseaux,Integer>("DegatSousBouclier"));
		columnCoqueMoyenne.setCellValueFactory(new PropertyValueFactory<Vaisseaux,Integer>("MoyenneSurCoque"));
		columnCoqueShootMax.setCellValueFactory(new PropertyValueFactory<Vaisseaux,Integer>("MaxShootCoque"));
		columnCoqueTotal.setCellValueFactory(new PropertyValueFactory<Vaisseaux,Integer>("DegatSurCoque"));
		columnTotal.setCellValueFactory(new PropertyValueFactory<Vaisseaux,Integer>("TotalDegat"));
		
		Validation.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				tri.recupHashMap().clear();

				StringTokenizer a = new StringTokenizer(Log.getText().trim(), ".");
				
				while(a.hasMoreTokens()) {
					tri.addOneLineFight(a.nextToken());
				}
				
				upgradeTable();	
			}
			
		});
		
	}
	
	public void upgradeTable() {
		HashMap<String,Vaisseaux> hash = tri.recupHashMap();
		tableauInformation.getItems().clear();
		listeVaisseaux.addAll(hash.values());
		tableauInformation.setItems(listeVaisseaux);
		System.out.println(listeVaisseaux.size());
	}

}
