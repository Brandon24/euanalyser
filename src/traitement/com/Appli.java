package traitement.com;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Appli extends Application {

	@SuppressWarnings("static-access")
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		Parent fxml = FXMLLoader.load(getClass().getResource("Frame.fxml"));
		final Scene scene = new Scene(fxml);
		primaryStage.setTitle("Analyser");
		primaryStage.setScene(scene);
		primaryStage.show();

	}

}
