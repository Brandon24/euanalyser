package traitement.com;
import java.util.HashMap;


public class Tri {
	
	private HashMap<String,Vaisseaux> hm = new HashMap<>();
	private Integer recupCoque=0;
	private Integer recupBouclier=0;
	
	
	public void addOneLineFight(String ajout) {
		
		ajout = ajout.replaceFirst("-", "");
		int test = ajout.indexOf("n�a pas");
		int mmq= ajout.indexOf("�t�");
		int retraitFight = ajout.indexOf("se retire");
		int joinFight = ajout.indexOf("rejoint le combat");
		// On Verifie que les lignes ne contiennent pas certains mot cl�s 
		// Car quand ces lignes contiennent certains de ces mots cl�s cela signifie
		// que il n'y a pas eu de tir dans ce log
		if(test==-1 && mmq == -1 && retraitFight == -1 && joinFight == -1) {
			parcours(ajout);
		}
	
	}
	
	public void parcours(String a) {
		// CoupeChaine[0] Le Vaisseaux qui tirent sur l'autre
		

		String nomVaisseaux = a.substring(0, a.indexOf("a touch�"));


		
		// MM[0] recup nom vsx ennemie 
		//MM[1] Reste Chaine
		
		String[] mm = a.split("caus�");
	

		char caractere[];

		System.out.println(mm[1].length());
		/**
		 * Si c'est un shoot sur coque
		 */
		if(mm[1].length()<30 && hm.containsKey(nomVaisseaux)) {
			
			caractere = mm[1].toCharArray();
			recupCoque = charToInt(caractere);
			
			hm.get(nomVaisseaux).upgradeDegatByRefresh(recupCoque);
			// Si le nom n'est pas present dans le hashMAP	
		}else if(mm[1].length()<30 && !hm.containsKey(nomVaisseaux)) {
			hm.put(nomVaisseaux, new Vaisseaux(nomVaisseaux,recupCoque));
		}
		
		/**
		 * Si c'est un shoot sur coque et bouclier
		 */
		if(hm.containsKey(nomVaisseaux) && mm[1].length() > 30) {
			String [] lobby = mm[1].split("coque");
			recupBouclier = charToInt(lobby[1].toCharArray());
			hm.get(nomVaisseaux).upgradeDegatByRefresh(recupCoque,recupBouclier);
				
		}else if(!hm.containsKey(nomVaisseaux) && mm[1].length() > 30) {
			String tableauBoublierDegat[]=mm[1].split(",");
				
			recupCoque = charToInt(tableauBoublierDegat[0].toCharArray());
			recupBouclier = charToInt(tableauBoublierDegat[1].toCharArray());

			hm.put(nomVaisseaux, new Vaisseaux(nomVaisseaux,recupCoque,recupBouclier));	
		}	
		
	}
		

	public HashMap<String, Vaisseaux> recupHashMap() {
		
		return this.hm;
	}
	
	/**
	 *  On recherche les d�g�ts inflig�s contre le vaisseau ennemi dans la string
	 * @param tab
	 * @return le nombre de d�g�ts
	 */
	public int charToInt(char tab[]) {
		char tableauNumber [] = {'0','1','2','3','4','5','6','7','8','9'};
		String concatene= "";
		
		for(int i = 0 ; i < tab.length; i++) {
			for(int b = 0; b < tableauNumber.length; b++) {
				if(tab[i]==tableauNumber[b]) {
					concatene += tab[i];
				}
			}
		
		}
		
		return Integer.parseInt(concatene);
	}

}
